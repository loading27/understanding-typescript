//string
let myName = 'Max';

//number
let myAge = 27;
myAge = 1.4;

//boolean
let hasHobbies = true;
hasHobbies = false;

//assign types 
let myRealAge: number;
myRealAge = 26;

//array type
let hobbies: string[] = ['cooking', 'sports'];
console.log(hobbies[0]);
hobbies = ['100'];

//tuples
let address: [string, number] = ["Superstreet", 99];

//enum
enum Color {
    Gray,
    Green,
    Blue
}
let myColor: Color = Color.Green;
console.log(myColor); //prints 1