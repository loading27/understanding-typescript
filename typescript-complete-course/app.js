"use strict";
//string
var myName = 'Max';
//number
var myAge = 27;
myAge = 1.4;
//boolean
var hasHobbies = true;
hasHobbies = false;
//assign types 
var myRealAge;
myRealAge = 26;
//array type
var hobbies = ['cooking', 'sports'];
console.log(hobbies[0]);
hobbies = ['100'];
//tuples
var address = ["Superstreet", 99];
//enum
var Color;
(function (Color) {
    Color[Color["Gray"] = 0] = "Gray";
    Color[Color["Green"] = 1] = "Green";
    Color[Color["Blue"] = 2] = "Blue";
})(Color || (Color = {}));
var myColor = Color.Green;
console.log(myColor); //prints 1
